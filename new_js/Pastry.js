class Pastry {
  constructor (name, ingridients){
    this._name = name;
    this._ingredients = ingridients ? ingridients : []; 
  }
  get name() {
    return this._name;
  }
  get ingridients() {
    return this._ingredients;
  }
  set ingridients(arr) {
    this._ingredients = this._ingredients.concat(arr);
  }
}