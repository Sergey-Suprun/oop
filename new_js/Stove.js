class Stove {
    constructor(name, pie) {
        this._name = name;
        this._pie = pie instanceof Pie ? pie : null;
        this._temperature = null;
        this._timer = null;
        this._timerEnds = false;
        this._doorState = false;
        this._baking = false;
        this._inStove = [];
    }
    get name() {
        return this._name;
    }
    get temperature() {
        return this._temperature;
    }
    set temperature(value) {
        if (value >= 0 && value <= 240) {
            this._temperature = value;
        }
    }
    get timer() {
        return this._timer;
    }
    set timer(value) {
        this._timer = value;
    }
    beep() {
        console.log(`Mmm, smells good! ${this._timer} seconds and your ${this._pie.name} pie is ready! BEEP!`);
    }
    get doorState() {
        return this._doorState;
    }
    openDoor() {
        this._doorState = false;
    }
    closeDoor() {
        this._doorState = true;
    }
    get baking() {
        return this._baking;
    }
    baking() {
        this._baking = value;
    }
    get fullReadiness() {
        return this._doorState && this._inStove.length;
    }
    checkWhatIsWrong() {
        if (!this.fullReadiness) {
            if (this._inStove.length == 0) {
                return new Error('Ther\'s nothing in the stove');
            } else if (!this._doorState) {
                return new Error('The door is open');
            } else {
                return new Error('Something really bad happened');
            }
        }
    }
    startBacking() {
        return new Promise((resolve, reject) => {
            this._baking = true;
            let timerInitialValue = this._timer;
            if (this.fullReadiness) {
                let timerID = setInterval(() => {
                    this._timer--;
                }, 1000);
                setTimeout(() => {
                    this.timer = timerInitialValue;
                    this._baking = false;
                    this._pie.state = true;
                    this.beep();
                    clearInterval(timerID);
                    resolve();
                }, this._timer * 1000);
            } else {
                reject(this.checkWhatIsWrong());
            }
        })
    }
    addToStove(pie) {
        this._inStove.push(pie)
    }
    checkStove() {
        return this._inStove;
    }
    removeFromStove() {
        return this._inStove.pop();
    }
}