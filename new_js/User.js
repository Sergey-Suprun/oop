class User {
    constructor(name,stove) {
        this._name = name;
        this._stove = stove;
    }
    get name() {
        return this._name;
    }
    get stove() {
        return this._stove;
    }
    set stove(stove) {
        this._stove = stove;
    }
}