class Pie extends Pastry {
    constructor(name,ingridients) {
        super(name,ingridients);
        this._state = false;
    }
    get state() {
        return this._state;
    }
    set state(value) {
        this._state = value
    }
}