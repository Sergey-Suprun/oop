//Pastry class
function Pastry(name, ingridients) {
  this._name = name; 
  this._ingredients = ingridients ? ingridients : []; 
}

Pastry.prototype.getName = function () {
  return this._name; 
}

Pastry.prototype.getIngredients = function () {
  return this._ingredients;
}

Pastry.prototype.setIngredients = function (arr) {
    this._ingredients = this._ingredients.concat(arr);
}