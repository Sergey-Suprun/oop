//User class
function User(name, stove) {
    this._name = name;
    this._stove = stove ? stove : null;
}

User.prototype.getName = function () {
    return this._name;
}
User.prototype.getStove = function () {
    return this._stove;
}
User.prototype.setStove = function (stove) {
    this._stove = stove;
}