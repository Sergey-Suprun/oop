// Pie class
function Pie(name, ingredients) {
  Pastry.call(this, name, ingredients);
  this._state = false;
}

Pie.prototype = Object.create(Pastry.prototype);
Pie.prototype.constructor = Pie;

Pie.prototype.getState = function () {
  return this._state;
}
Pie.prototype.setState = function (value) {
  this._state = value;
}