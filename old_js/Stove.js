// Stove class
function Stove(name, pie) {
  this._name = name;
  this._temperature = null;
  this._timer = null;
  this._timerEnds = true;
  this._doorState = false;
  this._baking = false;
  this._inStove = [];
  this._pie = pie instanceof Pie ? pie : null;
}

Stove.prototype.getName = function () {
  return this._name;
};

Stove.prototype.getTemperature = function () {
  return this._temperature;
};

Stove.prototype.setTemperature = function (value) {
  if (value >= 0 && value <= 240) {
    this._temperature = value;
  }
};

Stove.prototype.getTimer = function () {
  return this._timer;
};

Stove.prototype.setTimer = function (value) {
  this._timer = value;
};

Stove.prototype.beep = function () {
  console.log('Mmm, smells good! ' + this._timer + ' seconds and your ' + this._pie.getName() + ' pie is ready! BEEP!');
};

Stove.prototype.getDoorState = function () {
  return this._doorState;
};

Stove.prototype.openDoor = function () {
  this._doorState = false;
};

Stove.prototype.closeDoor = function () {
  this._doorState = true;
};

Stove.prototype.getBaking = function () {
  return this._baking;
};

Stove.prototype.setBaking = function (value) {
  this._baking = value;
};

Stove.prototype.getFullReadiness = function () {
  return this._doorState && this._inStove.length;
};

Stove.prototype.checkWhatIsWrong = function () {
  if (!this.getFullReadiness()) {
    if (this._inStove.length == 0) {
      return new Error('Ther\'s nothing in the stove');
    } else if (!this._doorState) {
      return new Error('The door is open');
    } else {
      return new Error('Something really bad happened');
    }
  }
};

Stove.prototype.startBaking = function (callback) {
  this._baking = true;
  var timerInitialValue = this._timer;
  if (this.getFullReadiness()) {
    var timer = this._timer * 1000;
    var timerId = setInterval(function () {
      this._timer--;
    }.bind(this), 1000);
    setTimeout(function () {
      this._timer = timerInitialValue;
      this._baking = false;
      this._pie.setState(true)
      this.beep();
      clearInterval(timerId);
      callback(null);
    }.bind(this), timer);
  } else {
      var err = this.checkWhatIsWrong();
      callback(err)
  }
};

Stove.prototype.addToStove = function (pie) {
  this._inStove.push(pie);
};

Stove.prototype.checkStove = function () {
  return this._inStove;
};

Stove.prototype.removeFromStove = function () {
  return this._inStove.pop();
};
