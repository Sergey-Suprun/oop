

interface IPastry {
    readonly name: string;
    ingridients: string[];
}

abstract class Pastry implements IPastry {
    protected _name: string;
    protected _ingridients: string[];

    public constructor(name: string, ingridients: string[]) {
        this._name = name;
        this._ingridients = ingridients;
    }
    public get name(): string {
        return this._name;
    }
    public get ingridients(): string[] {
        return this._ingridients;
    }
    public set ingridients(ingridients: string[]) {
        this._ingridients = ingridients;
    }
}