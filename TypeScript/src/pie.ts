interface IPie extends IPastry {
    state: boolean;
}
class Pie extends Pastry implements IPie {
    protected _state: boolean;

    public constructor(name: string, ingridients: string[]) {
        super(name, ingridients);
        this._state = false;
    }
    public get state(): boolean {
        return this._state;
    }
    public set state(value: boolean) {
        this._state = value;
    }
}