interface IList<T> {
    add(item: T): void;
    get(index: number): T | null;
    delete(index: number): void;
    toArray(): T[];
    clear(): void;
    size(): number;
}

class List<T> implements IList<T> {
    private _list: Array<T>;

    public constructor(value: T[]) {
        this._list = value;
    }
    public add(item: T): void {
        this._list.push(item)
    }
    public get(index: number): T | null {
        if (this._list[index]) {
            return this._list[index]
        } else {
            return null;
        }
    }
    public delete(index: number): void {
        delete this._list[index];
    }
    public toArray(): T[] {
        return this._list;
    }
    public clear(): void {
        this._list = [];
    }
    public size(): number {
        return this._list.length;
    }
}