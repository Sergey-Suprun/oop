interface IStove {
    readonly name: string;
    temperature: number;
    timer: number;
    doorState: boolean;

    startBaking(): Promise<void>;
    checkStove(): IPie[];
    addToStove(value: IPie): void;
    removeFromStove(): void;
}

class Stove implements IStove {
    protected _name: string;
    protected _temperature: number;
    protected _timer: number;
    protected _doorState: boolean;
    protected _baking: boolean;
    protected _inStove: IPie[];
    protected _pie: IPie;

    public constructor(name: string, pie: IPie) {
        this._name = name;
        this._pie = pie;
        this._temperature = 0;
        this._timer = 0;
        this._doorState = false;
        this._baking = false;
        this._inStove = [];
    }
    public get name(): string {
        return this._name;
    }
    public get temperature(): number {
        return this._temperature;
    }
    public set temperature(value: number) {
        this._temperature = value;
    }
    public get timer(): number {
        return this._timer;
    }
    public set timer(value: number) {
        this._timer = value;
    }
    public beep(): void {
        console.log(`Mmm, smells good! ${this._timer} seconds and your ${this._pie.name} pie is ready! BEEP!`);
    }
    public get doorState(): boolean {
        return this._doorState;
    }
    public set doorState(value: boolean) {
        this._doorState = value;
    }
    public get baking(): boolean {
        return this._baking;
    }
    public set baking(value: boolean) {
        this._baking = value;
    }
    public checkFullReadiness(): boolean {
        return Boolean(this._doorState && this._inStove.length);
    }
    public checkWhatIsWrong(): string {
        if (!this.checkFullReadiness()) {
            if (this._inStove.length == 0) {
                return "Ther's nothing in the stove";
            } else if (!this._doorState) {
                return "The door is open";
            } else {
                return "Something really bad happened";
            }
        } return "All fine!"
    }
    public startBaking(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this._baking = true;
            let timerInitialValue: number = this._timer;
            if (this.checkFullReadiness()) {
                let timerID: number = setInterval(() => {
                    this._timer--;
                }, 1000);
                setTimeout(() => {
                    this.timer = timerInitialValue;
                    this._baking = false;
                    this._pie.state = true;
                    this.beep();
                    clearInterval(timerID);
                    resolve();
                }, this._timer * 1000);
            } else {
                let err: string = this.checkWhatIsWrong();
                reject(new Error(err));
            }
        });
    }
    public checkStove(): IPie[] {
        return this._inStove;
    }
    public addToStove(pie: IPie): void {
        this._inStove.push(pie);
    }
    public removeFromStove(): void {
        this._inStove = [];
    }
}