
class User {
    protected _name: string;
    protected _stove: IStove;

    public constructor(name: string, stove: IStove) {
        this._name = name;
        this._stove = stove;
    }
    public get name(): string {
        return this._name;
    }
    public get stove(): IStove {
        return this._stove;
    }
    public set stove(stove: IStove) {
        this._stove = stove;
    }
}