"use strict";
class List {
    constructor(value) {
        this._list = value;
    }
    add(item) {
        this._list.push(item);
    }
    get(index) {
        if (this._list[index]) {
            return this._list[index];
        }
        else {
            return null;
        }
    }
    delete(index) {
        delete this._list[index];
    }
    toArray() {
        return this._list;
    }
    clear() {
        this._list = [];
    }
    size() {
        return this._list.length;
    }
}
class Pastry {
    constructor(name, ingridients) {
        this._name = name;
        this._ingridients = ingridients;
    }
    get name() {
        return this._name;
    }
    get ingridients() {
        return this._ingridients;
    }
    set ingridients(ingridients) {
        this._ingridients = ingridients;
    }
}
class Pie extends Pastry {
    constructor(name, ingridients) {
        super(name, ingridients);
        this._state = false;
    }
    get state() {
        return this._state;
    }
    set state(value) {
        this._state = value;
    }
}
class User {
    constructor(name, stove) {
        this._name = name;
        this._stove = stove;
    }
    get name() {
        return this._name;
    }
    get stove() {
        return this._stove;
    }
    set stove(stove) {
        this._stove = stove;
    }
}
class Stove {
    constructor(name, pie) {
        this._name = name;
        this._pie = pie;
        this._temperature = 0;
        this._timer = 0;
        this._doorState = false;
        this._baking = false;
        this._inStove = [];
    }
    get name() {
        return this._name;
    }
    get temperature() {
        return this._temperature;
    }
    set temperature(value) {
        this._temperature = value;
    }
    get timer() {
        return this._timer;
    }
    set timer(value) {
        this._timer = value;
    }
    beep() {
        console.log(`Mmm, smells good! ${this._timer} seconds and your ${this._pie.name} pie is ready! BEEP!`);
    }
    get doorState() {
        return this._doorState;
    }
    set doorState(value) {
        this._doorState = value;
    }
    get baking() {
        return this._baking;
    }
    set baking(value) {
        this._baking = value;
    }
    checkFullReadiness() {
        if (this._doorState && this._inStove.length) {
            return true;
        }
        else {
            return false;
        }
    }
    checkWhatIsWrong() {
        if (!this.checkFullReadiness()) {
            if (this._inStove.length == 0) {
                return "Ther's nothing in the stove";
            }
            else if (!this._doorState) {
                return "The door is open";
            }
            else {
                return "Something really bad happened";
            }
        }
        return "All fine!";
    }
    startBaking() {
        return new Promise((resolve, reject) => {
            this._baking = true;
            let timerInitialValue = this._timer;
            if (this.checkFullReadiness()) {
                let timerID = setInterval(() => {
                    this._timer--;
                }, 1000);
                setTimeout(() => {
                    this.timer = timerInitialValue;
                    this._baking = false;
                    this._pie.state = true;
                    this.beep();
                    clearInterval(timerID);
                    resolve();
                }, this._timer * 1000);
            }
            else {
                let err = this.checkWhatIsWrong();
                reject(new Error(err));
            }
        });
    }
    checkStove() {
        return this._inStove;
    }
    addToStove(pie) {
        this._inStove.push(pie);
    }
    removeFromStove() {
        this._inStove = [];
    }
}
// const pie: IPie = new Pie('bluberry', ['flavor', 'shugar', 'berries', 'water']);
// const stove: IStove = new Stove('Electrolux', pie)
// const user: User = new User('John', stove)
// user.stove.addToStove(pie);
// user.stove.timer = 5;
// user.stove.doorState = true;
// user.stove.temperature = 200;
// async function baking(): Promise<void> {
//     try {
//         await user.stove.startBaking();
//         user.stove.timer = 7;
//         // user.stove.doorState = false;
//         await user.stove.startBaking();
//         user.stove.timer = 3;
//         await user.stove.startBaking();
//     } catch (err) {
//         console.log(err);
//     } finally {
//         console.log('hooh');
//     }
// }
// baking();
const list = new List([]);
list.add(2);
list.add(5);
list.add(6);
list.add(7);
console.log(list.get(1));
list.delete(1);
console.log(list.get(1));
console.log(list.toArray());
console.log(list.size());
list.clear();
console.log(list.size());
